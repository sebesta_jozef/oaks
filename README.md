# BE - Oaks

### Install dependencies

```
npm install
```

### start project (on localhost:3000)

``
npm start
``

### Graphql web UI is then available at localhost:3000/graphql

## Main queries

### list of startup phases with attributes that determine whether given phase is finished and unlocked:
```
query {startupPhases {
  id
  startup {
    id
    name
  }
  isFinished
  isUnlocked
}}
```

### list of startup phase tasks with attributes that determine whether given task is checked:
```
query {startupPhaseTasks {
  id
  task {
    id
    name
  }
  startupPhase {
    id
    startup {
      id
      name
    }
  }
  isChecked
}}
```

## Main mutations

### update startup phase with attribute isFinished and isUnlocked

```
mutation {
  updateStartupPhase(input: {id:2, isFinished:true, isUnlocked:false}) {
    id
  }
}

```

### update startup phase task (if the task is done - attribute isChecked should be true)

```
mutation {
  updateStartupPhaseTask(input: {id:5, isChecked:true}) {
    id
  }
}

```
