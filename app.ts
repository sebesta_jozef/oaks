import express from 'express';
import graphqlHTTP from 'express-graphql';
import {makeExecutableSchema} from 'graphql-tools';
import {StartupService} from "./src/service/StartupService";
import {StartupPhaseService} from "./src/service/StartupPhaseService";
import {StartupPhaseTaskService} from "./src/service/StartupPhaseTaskService";

const app: express.Application = express();
const port = 3000;

let startupService = new StartupService();
let startupPhaseService = new StartupPhaseService();
let startupTaskService = new StartupPhaseTaskService();

let typeDefs = startupService.configTypeDefs();
typeDefs += startupPhaseService.configTypeDefs();
typeDefs += startupTaskService.configTypeDefs();

let resolvers = startupService.configResolvers();
startupPhaseService.configResolvers(resolvers);
startupTaskService.configResolvers(resolvers);


app.use(
    '/graphql',
    graphqlHTTP({
        schema: makeExecutableSchema({typeDefs, resolvers}),
        graphiql: true
    })
);
app.listen(port, () => console.log(`Node Graphql API listening on port ${port}!`));