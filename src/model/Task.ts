export class Task {
    private id: Number = 0;
    private name: String = '';

    constructor(taskId: Number,
                taskName: String) {
        this.id = taskId;
        this.name = taskName;
    }
}

export default Task