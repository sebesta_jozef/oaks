export class StartupPhase {
    private id: number = 0;
    private phase: any;
    private startup: any;
    private isFinished: boolean = false;
    private isUnlocked: boolean = false;

    constructor(startupPhaseId: number,
                startupPhase: any,
                startup: any,
                startupPhaseIsFinished: boolean,
                startupPhaseIsUnlocked: boolean) {
        this.id = startupPhaseId;
        this.phase = startupPhase;
        this.startup = startup;
        this.isFinished = startupPhaseIsFinished;
        this.isUnlocked = startupPhaseIsUnlocked;
    }
}

export default StartupPhase;