
import Phase from './Phase';

const phases: Array<Phase> = [];

phases.push(new Phase(1, 'Foundation', 'FOUNDATION'))
phases.push(new Phase(2, 'Discovery', 'DISCOVERY'))
phases.push(new Phase(3, 'Delivery', 'DELIVERY'))

export default phases
