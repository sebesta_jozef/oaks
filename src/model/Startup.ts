export class Startup {
    private id: number = 0;
    private name: string = '';

    constructor(startupId: number,
                startupName: string) {
        this.id = startupId;
        this.name = startupName;
    }
}

export default Startup;
