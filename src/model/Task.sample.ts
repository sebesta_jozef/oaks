
import Task from './Task';

const tasks: Array<Task> = [];

tasks.push(new Task(1, 'Setup virtual office'))
tasks.push(new Task(2, 'Set mission & vision'))
tasks.push(new Task(3, 'Select business name'))
tasks.push(new Task(4, 'Buy domains'))
tasks.push(new Task(5, 'Create roadmap'))
tasks.push(new Task(6, 'Competitor analysis'))
tasks.push(new Task(7, 'Release marketing website'))
tasks.push(new Task(8, 'Release MVP'))

export default tasks
