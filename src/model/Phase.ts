export class Phase {
    private id: Number = 0;
    private name: String = '';
    private phaseType: String = '';

    constructor(phaseId: Number,
                phaseName: String,
                phaseType: String,
    ) {
        this.id = phaseId;
        this.name = phaseName;
        this.phaseType = phaseType;
    }
}
export default Phase;