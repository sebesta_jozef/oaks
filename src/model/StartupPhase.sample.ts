import StartupPhase from './StartupPhase';
import startupSample from "./Startup.sample";
import phaseSample from "./Phase.sample";

const startupPhases: Array<StartupPhase> = [];

startupPhases.push(new StartupPhase(1, phaseSample[0], startupSample[0], true, false))
startupPhases.push(new StartupPhase(2, phaseSample[1], startupSample[0], false, true))
startupPhases.push(new StartupPhase(3, phaseSample[2], startupSample[0], false, false))

export default startupPhases