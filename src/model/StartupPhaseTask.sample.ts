import StartupPhaseTask from './StartupPhaseTask';
import startupPhaseSample from "./StartupPhase.sample";
import taskSample from "./Task.sample";

const startupPhaseTasks: Array<StartupPhaseTask> = [];

startupPhaseTasks.push(new StartupPhaseTask(1, startupPhaseSample[0], taskSample[0], true))
startupPhaseTasks.push(new StartupPhaseTask(2, startupPhaseSample[0], taskSample[1], true))
startupPhaseTasks.push(new StartupPhaseTask(3, startupPhaseSample[0], taskSample[2], true))
startupPhaseTasks.push(new StartupPhaseTask(4, startupPhaseSample[0], taskSample[3], true))

startupPhaseTasks.push(new StartupPhaseTask(5, startupPhaseSample[1], taskSample[4], true))
startupPhaseTasks.push(new StartupPhaseTask(6, startupPhaseSample[1], taskSample[5], false))

startupPhaseTasks.push(new StartupPhaseTask(7, startupPhaseSample[2], taskSample[6], false))
startupPhaseTasks.push(new StartupPhaseTask(8, startupPhaseSample[2], taskSample[7], false))

export default startupPhaseTasks;