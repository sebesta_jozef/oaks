
import Startup from './Startup';

const startups: Array<Startup> = [];

startups.push(new Startup(1, 'Startup A'))
startups.push(new Startup(2, 'Startup B'))

export default startups