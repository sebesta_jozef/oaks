export class StartupPhaseTask {
    private id: number = 0;
    private startupPhase: any;
    private task: any;
    private isChecked: boolean = false;

    constructor(startupPhaseTaskId: number,
                startupPhase: any,
                task: any,
                isChecked: boolean) {
        this.id = startupPhaseTaskId;
        this.startupPhase = startupPhase;
        this.task = task;
        this.isChecked = isChecked;
    }
}

export default StartupPhaseTask;