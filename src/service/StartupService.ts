import startupSample from "../model/Startup.sample";


export class StartupService {

    public startups: any = startupSample;
    private generatedId: number = startupSample.length + 1;

    configTypeDefs() {
        return `
          type Startup {
            name: String!,
            id: ID!
          } 
          
          input StartupInput {
            name: String!
          }
          
          type Query {
            startups: [Startup!]
          }
          
          type Mutation {
            createStartup(input: StartupInput): Startup!
          }`;
    }

    configResolvers() {
        return {
            Query: {
                startups: () => {
                    return this.startups;
                }
            },

            Mutation: {
                createStartup: (root:any, data: {input: {name: string}}) => {
                    const startup = {name: data.input.name, id: this.generatedId++}
                    this.startups.push(startup);
                    return startup;
                }
            }
    }}
}
