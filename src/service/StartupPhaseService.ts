import phaseSample from "../model/Phase.sample";
import startupPhaseSample from "../model/StartupPhase.sample";

export class StartupPhaseService {

    public phases: any = phaseSample;
    public startupPhases: any = startupPhaseSample;
    private generatedPhaseId: number = phaseSample.length + 1;
    private generatedStartupPhaseId: number = startupPhaseSample.length + 1;

    configTypeDefs() {
        return `
          enum PhaseType {
            FOUNDATION
            DISCOVERY
            DELIVERY
          }
          
          type Phase {
            name: String!,
            id: ID!,
            phaseType: PhaseType!
          }
          
          type StartupPhase {
            id: ID!,
            phase: Phase!,
            startup: Startup!,
            isFinished: Boolean!,
            isUnlocked: Boolean!
          }
          
          input CreatePhaseInput {
            name: String!,
            phaseType: PhaseType
          }
          
          input CreateStartupPhaseInput {
            phase: ID!,
            startup: ID!,
          }
          
          input UpdateStartupPhaseInput {
            id: Int!,
            isFinished: Boolean!,
            isUnlocked: Boolean!
          }
          
          extend type Mutation {
            createPhase(input: CreatePhaseInput): Phase!,
            updateStartupPhase(input: UpdateStartupPhaseInput): StartupPhase!
            createStartupPhase(input: CreateStartupPhaseInput): StartupPhase!
          }
          
          extend type Query {
            phases: [Phase!]
            startupPhases: [StartupPhase!]
          }`
    }

    configResolvers(resolvers: any) {
        resolvers.Mutation.createPhase = (root:any, data: {input: {name: string, phaseType: string}}) => {
            const phase = {name: data.input.name, phaseType: data.input.phaseType, id: this.generatedPhaseId++}
            this.phases.push(phase);
            return phase;
        };

        resolvers.Query.phases = () => {
            return this.phases;
        };

        resolvers.Query.startupPhases = () => {
            return this.startupPhases;
        };

        resolvers.Mutation.createStartupPhase = (root:any, data: {input: {phase: any, startup: any}}) => {
            const startupPhase = {startup: data.input.startup, phase: data.input.phase, isFinished: false, isUnlocked: false, id: this.generatedStartupPhaseId++}
            this.startupPhases.push(startupPhase);
            return startupPhase;
        };

        resolvers.Mutation.updateStartupPhase = (root:any, data: {input: {id: number, isFinished: boolean, isUnlocked: boolean}}) => {
            const exists = this.startupPhases.some((startupPhaseTask: any) => startupPhaseTask.id === data.input.id)
            if (!exists) {
                throw Error('startup phase with given ID does not exist')
            }

            this.startupPhases.map((startupPhase: any) => {
                if (startupPhase.id === data.input.id) {
                    startupPhase.isFinished = data.input.isFinished;
                    startupPhase.isUnlocked = data.input.isUnlocked;
                    return startupPhase;
                }
            });
            return this.startupPhases.filter((startupPhaseTask: any) => startupPhaseTask.id === data.input.id) [0];
        };
    }
}
