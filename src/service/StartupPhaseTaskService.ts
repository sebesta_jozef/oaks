import startupPhaseTaskSample from "../model/StartupPhaseTask.sample";

export class StartupPhaseTaskService {

    public startupPhaseTasks: any = startupPhaseTaskSample;
    private generatedStartupPhaseTaskId: number = startupPhaseTaskSample.length + 1;

    configTypeDefs() {
        return `
          type Task {
            name: String!,
            id: Int!,
          }
          
          input CreateStartupPhaseTask {
            startupPhase: ID!,
            task: ID!
          }
          
          input UpdateStartupPhaseTask {
            id: Int!,
            isChecked: Boolean!
          }
          
          type StartupPhaseTask {
            id: ID!,
            startupPhase: StartupPhase!,
            task: Task!,
            isChecked: Boolean,
          }
          
          extend type Query {
            startupPhaseTasks: [StartupPhaseTask!]
            tasks: [Task!]
          }
          
          extend type Mutation {
               createStartupPhaseTask(input: CreateStartupPhaseTask): StartupPhaseTask
               updateStartupPhaseTask(input: UpdateStartupPhaseTask): StartupPhaseTask
          }
          `
    }

    configResolvers(resolvers: any) {
        resolvers.Query.startupPhaseTasks = () => {
            return this.startupPhaseTasks;
        };

        resolvers.Mutation.updateStartupPhaseTask = (root:any, data: {input: {id: number, isChecked: boolean}}) => {
            const exists = this.startupPhaseTasks.some((startupPhaseTask: any) => startupPhaseTask.id === data.input.id)
            if (!exists) {
                throw Error('startup phase task with given ID does not exist')
            }
            this.startupPhaseTasks.map((startupPhaseTask: any) => {
                if (startupPhaseTask.id === data.input.id) {
                    startupPhaseTask.isChecked = data.input.isChecked;
                    return startupPhaseTask;
                }
            });
            return this.startupPhaseTasks.filter((startupPhaseTask: any) => startupPhaseTask.id === data.input.id) [0];
        };


        resolvers.Mutation.createStartupPhaseTask = (root:any, data: {input: {task: any, startupPhase: any}}) => {
            const newStartupPhaseTask = { id: this.generatedStartupPhaseTaskId++, task: data.input.task, startupPhase: data.input.startupPhase, isChecked:false}
            this.startupPhaseTasks.push(newStartupPhaseTask);
            return this.startupPhaseTasks;
        };
    }
}
